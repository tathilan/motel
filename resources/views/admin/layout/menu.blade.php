<nav class="navbar-default navbar-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav" id="main-menu">

            <li>
                <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>Category Article<span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/categories">List </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>Article <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/articles">List </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>Users <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">List </a>
                    </li>

                </ul>
            </li>
            <li>
                <a href="#" class="waves-effect waves-dark"><i class="fa fa-sitemap"></i>Admin <span
                        class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="#">List </a>
                    </li>
                </ul>
            </li>

        </ul>

    </div>

</nav>
