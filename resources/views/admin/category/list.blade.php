@extends('admin.layout.index')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">

                <div class="card">
                    <div class="card-action">
                        Category Tables <br>
                        <button class="add-modal btn btn-success" data-target="#addModal" data-toggle="modal"
                                style="float:right">
                            <span class="glyphicon glyphicon-eye-open"></span>Add category
                        </button>
                        <form action="admin/categories/search" method="" class="navbar-form navbar-left"
                              role="search">
                            <div class="form-group">
                                <input type="text" name="keyword" class="form-control" placeholder="Search">
                            </div>
                            <button type="submit" class="btn btn-default"
                                    style="background-color: #3399FF; color: white;">Tìm kiếm
                            </button>
                        </form>
                    </div>
                    <br><br>
                    <div class="card-content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($categories as $c_infor)
                                    <tr class="odd gradeX" id="indexcategory{{$c_infor->id}}">
                                        <td>{{$c_infor->id}}</td>
                                        <td><a href="/admin/articles/category/{{$c_infor->id}}">{{$c_infor->name}}</a>
                                        </td>
                                        <td>
                                            <button class="edit-modal center btn btn-info" data-id="{{$c_infor->id}}"
                                                    data-name="{{$c_infor->name}}" data-target="#editModal"
                                                    data-toggle="modal">
                                                <span class="glyphicon glyphicon-edit"></span> Edit
                                            </button>
                                            <button class="delete-modal center btn btn-danger"
                                                    data-id="{{$c_infor->id}}" data-name="{{$c_infor->name}}"
                                                    data-target="#deleteModal" data-toggle="modal">
                                                <span class="glyphicon glyphicon-trash"></span> Delete
                                            </button>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            {{ $categories->links() }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
     <!--End Advanced Tables -->

    @include('admin.category.add')
    @include('admin.category.edit')
    @include('admin.category.delete')
    @include('admin.category.html')

    <!--AJAX CRUD-->
    <script type="text/javascript" src="js/ajaxcategory.js"></script>

@endsection
