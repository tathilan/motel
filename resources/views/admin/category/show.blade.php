@extends('admin.layout.index')
@section('content')
    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-action">
                            Article Tables<br>
                            <a href="/admin/category">
                                <button class=" btn btn-success" style="float:right">
                                    <span class="glyphicon"></span>Back to Category
                                </button>
                            </a>
                            <button class="add-modal btn btn-success" data-target="#addModal" data-toggle="modal"
                                    style="float:right">
                                <span class="glyphicon glyphicon-eye-open"></span>Add article
                            </button>
                            <form action="{{ route('articles.index') }}" method="" class="navbar-form navbar-left"
                                  role="search">

                                <div class="form-group">
                                    <input type="text" name="search" class="form-control" placeholder="Search">
                                </div>
                                <button type="submit" class="btn btn-default"
                                        style="background-color: #3399FF; color: white;">Tìm kiếm
                                </button>
                            </form>
                        </div>
                        <br><br>
                        <div class="card-content">
                            <div class="table-responsive">

                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Category</th>
                                        <th>Title</th>
                                        <th>Content</th>
                                        <th>Address</th>
                                        <th>Status</th>
                                        <th>Phone Number</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($articles as $a_infor)
                                        <tr class="odd gradeX" id="indexArticle{{$a_infor->id}}">
                                            <td>{{$a_infor->id}}</td>
                                            <td>{{$a_infor->categoryarticle['name']}}</td>
                                            <td>{{$a_infor->title}}</td>
                                            <td>{{$a_infor->content}}</td>
                                            <td>{{$a_infor->address}}</td>
                                            <td>{{$a_infor->status}}</td>
                                            <td>{{$a_infor->phonenumber}}</td>
                                            <td>
                                                <button class="center btn btn-info edit-modal"
                                                        data-id="{{$a_infor->id}}"
                                                        data-category="{{$a_infor->category_id}}"
                                                        data-title="{{$a_infor->title}}"
                                                        data-content="{{$a_infor->content}}"
                                                        data-address="{{$a_infor->address}}"
                                                        data-phonenumber="{{$a_infor->phonenumber}}"
                                                        data-status="{{$a_infor->status}}" data-target="#editModal"
                                                        data-toggle="modal">
                                                    <span class="glyphicon glyphicon-edit"></span>Edit
                                                </button>
                                                <button class="delete-modal center btn btn-danger"
                                                        data-id="{{$a_infor->id}}"
                                                        data-category="{{$a_infor->category_id}}"
                                                        data-title="{{$a_infor->title}}"
                                                        data-content="{{$a_infor->content}}"
                                                        data-address="{{$a_infor->address}}"
                                                        data-phonenumber="{{$a_infor->phonenumber}}"
                                                        data-status="{{$a_infor->status}}" data-target="#deleteModal"
                                                        data-toggle="modal">
                                                    <span class="glyphicon glyphicon-trash"></span>Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('admin.article.add')
    @include('admin.article.edit')
    @include('admin.article.delete')


    <!--AJAX CRUD-->
    <script type="text/javascript" src="js/ajaxarticle.js"></script>
@endsection
