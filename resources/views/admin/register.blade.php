<!DOCTYPE html>
<html>

<head>
    <base href="{{asset('')}}">
    <title>ADMIN REGISTER </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
</head>
<body>
<h1>RAINBOW REGISTER FORM</h1>

<div class="w3layoutscontaineragileits">
    <h2>Register here</h2>
    <div class="panel-body">
        @if(count($errors) >0)
            <div style="color: red;">
                @foreach($errors->all() as $err)
                    {{$err}}<br>

                @endforeach
            </div>
        @endif
        @if(session('notification'))
            <div class="alert alert-success">
                {{session('notification')}}<br>
            </div>
        @endif
        <form action="admin/register" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="text" name="name" placeholder="NAME">
            <input type="email" name="email" placeholder="EMAIL">
            <input type="password" name="password" placeholder="PASSWORD">
            <input type="password" name="passwordAgain" placeholder="PASSWORD AGAIN">
            <div class="aitssendbuttonw3ls">
                <input type="submit" value="REGISTER">
                <p> To login account <span>→</span> <a class="w3_play_icon1" href="admin/login">LOGIN</a></p>
                <div class="clear"></div>
            </div>
        </form>
    </div>

</body>

</html>
