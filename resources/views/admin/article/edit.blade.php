<!-- Modal form to edit a form -->
<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: green">Update a article</h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_edit" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="category_id">Category:</label>
                        <div class="col-sm-10">
                            <select class="form-control" id="category_edit" name="category_id">
                                @foreach($categories as $c_infor)
                                    <option value="{{$c_infor->id}}" id="category_id">
                                        {{$c_infor->id}}-{{$c_infor->name}}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Title:</label>
                        <div class="col-sm-10">
                            <textarea type="text" class="form-control" id="title_edit" autofocus></textarea>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Content:</label>
                        <div class="col-sm-10">
                    <textarea type="text" class="form-control" id="content_edit" cols="40" rows="5"
                              autofocus></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="address">Address:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="address_edit" autofocus>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="status">Status:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="status_edit" autofocus>
                            <small>0: not post yet , 1: posted</small>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="phonenumber">Phone number:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="phonenumber_edit" autofocus>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary edit" data-dismiss="modal">
                            <span class='glyphicon glyphicon-check'></span> Edit
                        </button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">
                            <span class='glyphicon glyphicon-remove'></span> Close
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
