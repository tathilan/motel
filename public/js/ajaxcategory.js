$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
//add a category
$(document).on('click', '.add-modal', function () {
    $('#name_add').val('');
});
$('.modal-footer').on('click', '.add', function () {
    $.ajax({
        type: 'POST',
        url: 'admin/categories',
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('#name_add').val()
        },
        success: function (data) {
            toastr.success('Successfully added category!', 'Success Alert', {timeOut: 3000});
            // var html = $('#indeacategory'+data.id).load('html.blade.php');
            var html = $('#html').html();
            html += '<tr class="odd gradeX" id="indexcategory' + data.id + '">';
            html += '<td>' + data.id + '</td> ';
            html += '<td><a href="/admin/articles/category/' + data.id + '">' + data.name + '</a></td> ';
            html += '<td><button class="edit-modal center btn btn-info" data-id="' + data.id + '" data-name="' + data.name + '" data-target="#editModal" data-toggle="modal">' +
                '<span class="glyphicon glyphicon-edit"></span> Edit</button>\n';
            html += '<button class="delete-modal center btn btn-danger" data-id="' + data.id + '" data-name="' + data.name + '" data-target="#deleteModal" data-toggle="modal">' +
                '<span class="glyphicon glyphicon-trash"></span> Delete</button></td></tr>';
            $('#dataTables-example').append(html);

        },
    })
        .fail(function (data) {
            setTimeout(function () {
                $.each(data.responseJSON.errors, function (key, value) {
                    toastr.error(value, 'Error Alert', {timeOut: 3000});
                }, 500);
            });
        });

});
//edit a category
$(document).on('click', '.edit-modal', function () {
    $('#id_edit').val($(this).data('id'));
    $('#name_edit').val($(this).data('name'));
    id = $('#id_edit').val();

});
$('.modal-footer').on('click', '.edit', function () {

    $.ajax({
        type: 'PUT',
        url: 'admin/categories/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
            'name': $('#name_edit').val(),
        },
        success: function (data) {
            toastr.success('Successfully updated a category article!', {timeOut: 3000});
            var html = "";
            html += '<tr class="odd gradeX" id="indexcategory' + data.id + '">';
            html += '<td>' + data.id + '</td>';
            html += '<td><a href="/admin/articles/category/' + data.id + '">' + data.name + '</a></td>';
            html += '<td><button class="edit-modal center btn btn-info" data-id="' + data.id + '" data-name="' + data.name + '" data-target="#editModal" data-toggle="modal">' +
                '<span class="glyphicon glyphicon-edit"></span> Edit</button>\n';
            html += '<button class="delete-modal center btn btn-danger" data-id="' + data.id + '" data-name="' + data.name + '" data-target="#deleteModal" data-toggle="modal">' +
                '<span class="glyphicon glyphicon-trash"></span> Delete</button></td></tr>';
            $('#indexcategory' + data.id).replaceWith(html);
        },
    }).fail(function (data) {
        setTimeout(function () {
            $.each(data.responseJSON.errors, function (key, value) {
                toastr.error(value, 'Error Alert', {timeOut: 3000});
            }, 500);
        });
    });
});
// delete a category
$(document).on('click', '.delete-modal', function () {
    $('#id_delete').val($(this).data('id'));
    $('#name_delete').val($(this).data('name'));
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function () {
    $.ajax({
        type: 'DELETE',
        url: 'admin/categories/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            toastr.success('Successfully deleted category!', 'Success Alert', {timeOut: 3000});
            $('#indexcategory' + data['id']).remove();
        }
    });
});
