$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});

// Add a article

$(document).on('click', '.add-modal', function () {
    $('#title_add').val('');
    $('#content_add').val('');
    $('#address_add').val('');
    $('#phonenumber_add').val('');
    $('#status_add').val('');
});
$('.modal-footer').on('click', '.add', function () {
    $.ajax({
        type: 'POST',
        url: 'admin/articles',
        data: {
            '_token': $('input[name=_token]').val(),
            'category_id': $('#category_add').val(),
            'title': $('#title_add').val(),
            'content': $('#content_add').val(),
            'address': $('#address_add').val(),
            'phonenumber': $('#phonenumber_add').val(),
            'status': $('#status_add').val(),
        },
        success: function (data) {
            toastr.success('Successfully added a article!', 'Success Alert', {timeOut: 3000});
            var html = "";
            html += '<tr class="odd gradeX" id="indexArticle' + data.article.id + '">';
            html += '<td>' + data.article.id + '</td>';
            html += '<td><a href="admin/articles/category/' + data.article.category_id + '">' + data.categories[(data.article.category_id - 1)].name + '</a></td>';
            html += '<td>' + data.article.title + '</td>';
            html += '<td>' + data.article.content + '</td>';
            html += '<td>' + data.article.address + '</td>';
            html += '<td>' + data.article.status + '</td>';
            html += '<td>' + data.article.phonenumber + '</td>';
            html += '<td><button class="center btn btn-info edit-modal"  data-id="' + data.article.id + '" data-category="' + data.article.category_id + '"  data-title="' + data.article.title + '" data-content="' + data.article.content + '"  data-address="' + data.article.address + '" data-phonenumber="' + data.article.phonenumber + '" data-status="' + data.article.status + '" data-target="#editModal" data-toggle="modal">';
            html += '<span class="glyphicon glyphicon-edit"></span>Edit</button>';
            html += '<button class="center btn btn-danger delete-modal"  data-id="' + data.article.id + '" data-category="' + data.article.category_id + '"  data-title="' + data.article.title + '" data-content="' + data.article.content + '"  data-address="' + data.article.address + '" data-phonenumber="' + data.article.phonenumber + '" data-status="' + data.article.status + '" data-target="#deleteModal" data-toggle="modal">';
            html += '<span class="glyphicon glyphicon-trash"></span>Delete</button></td></tr>';
            $('#dataTables-example').append(html);
        },
    }).fail(function (data) {
        setTimeout(function () {
            $.each(data.responseJSON.errors, function (key, value) {
                toastr.error(value, 'Error Alert', {timeOut: 2000});
            }, 500);
        });
    });
});


// delete a article
$(document).on('click', '.delete-modal', function () {
    $('#id_delete').val($(this).data('id'));
    $('#title_delete').val($(this).data('title'));
    $('#content_delete').val($(this).data('content'));
    id = $('#id_delete').val();
});
$('.modal-footer').on('click', '.delete', function () {
    $.ajax({
        type: 'DELETE',
        url: 'admin/articles/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
        },
        success: function (data) {
            toastr.success('Successfully deleted article!', 'Success Alert', {timeOut: 5000});
            $('#indexArticle' + data.article['id']).remove();
        }
    });
});

//edit a category
$(document).on('click', '.edit-modal', function () {
    $('#id_edit').val($(this).data('id'));
    $('#category_edit').val($(this).data('category'));
    $('#title_edit').val($(this).data('title'));
    $('#content_edit').val($(this).data('content'));
    $('#address_edit').val($(this).data('address'));
    $('#status_edit').val($(this).data('status'));
    $('#phonenumber_edit').val($(this).data('phonenumber'));
    id = $('#id_edit').val();

});
$('.modal-footer').on('click', '.edit', function () {
    $.ajax({
        type: 'PUT',
        url: 'admin/articles/' + id,
        data: {
            '_token': $('input[name=_token]').val(),
            'category_id': $('#category_edit').val(),
            'title': $('#title_edit').val(),
            'content': $('#content_edit').val(),
            'address': $('#address_edit').val(),
            'status': $('#status_edit').val(),
            'phonenumber': $('#phonenumber_edit').val(),
        },
        success: function (data) {
            toastr.success('Successfully updated a article!', {timeOut: 5000});
            var html = "";
            html += '<tr class="odd gradeX" id="indexArticle' + data.article.id + '">';
            html += '<td>' + data.article.id + '</td>';
            html += '<td><a href="admin/articles/category/' + data.article.category_id + '">' + data.categories[(data.article.category_id - 1)].name + '</a></td>';
            html += '<td>' + data.article.title + '</td>';
            html += '<td>' + data.article.content + '</td>';
            html += '<td>' + data.article.address + '</td>';
            html += '<td>' + data.article.status + '</td>';
            html += '<td>' + data.article.phonenumber + '</td>';
            html += '<td><button class="center btn btn-info edit-modal"  data-id="' + data.article.id + '" data-category="' + data.article.category_id + '"  data-title="' + data.article.title + '" data-content="' + data.article.content + '"  data-address="' + data.article.address + '" data-phonenumber="' + data.article.phonenumber + '" data-status="' + data.article.status + '" data-target="#editModal" data-toggle="modal">';
            html += '<span class="glyphicon glyphicon-edit"></span>Edit</button>';
            html += '<button class="center btn btn-danger delete-modal"  data-id="' + data.article.id + '" data-category="' + data.article.category_id + '"  data-title="' + data.article.title + '" data-content="' + data.article.content + '"  data-address="' + data.article.address + '" data-phonenumber="' + data.article.phonenumber + '" data-status="' + data.article.status + '" data-target="#deleteModal" data-toggle="modal">';
            html += '<span class="glyphicon glyphicon-trash"></span>Delete</button></td></tr>';
            $('#indexArticle' + data.article.id).replaceWith(html);

        },
    }).fail(function (data) {
        setTimeout(function () {
            $.each(data.responseJSON.errors, function (key, value) {
                toastr.error(value, 'Error Alert', {timeOut: 5000});
            }, 500);
        });
    });
});
