<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});
Route::group(['prefix'=>'admin','middleware'=>'adminLogin'],function()
{
    Route::resource('categories','CategoryController');
	Route::resource('articles','ArticleController');
	Route::get('/articles/category/{id}','CategoryController@showArticle');
});

Route::get('admin/register','AccountController@getRegisterAdmin');
Route::post('admin/register','AccountController@postRegisterAdmin');
Route::get('admin/login','AccountController@getLoginAdmin');
Route::post('admin/login','AccountController@postLoginAdmin');

