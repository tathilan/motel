<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    //
    protected $table = 'articles';
    protected $fillable = ['id', 'category_id', 'title', 'content', 'address', 'status', 'phonenumber'];

    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }

    public function showArticle()
    {
        $articles = Article::paginate(5);
        return $articles;
    }

    public function addArticle($input)
    {
        $article = new Article();
        $article->id = $input->id;
        $article->category_id = $input->category_id;
        $article->title = $input->title;
        $article->content = $input->content;
        $article->address = $input->address;
        $article->phonenumber = $input->phonenumber;
        $article->status = $input->status;
        $article->save();
        return $article;
    }

    public function updateArticle($input, $id)
    {
        $article = Article::findOrFail($id);
        $article->category_id = $input->category_id;
        $article->title = $input->title;
        $article->content = $input->content;
        $article->address = $input->address;
        $article->status = $input->status;
        $article->phonenumber = $input->phonenumber;
        $article->save();

        return $article;
    }

    public function deleteArticle($id)
    {
        $article = Article::findOrFail($id);
        $article->delete();
        return $article;
    }

    public function searchArticle($key)
    {
        $articles = Article::select('articles.*')
            ->join('category_articles', 'articles.category_id', '=', 'category_articles.id')
            ->where('articles.id', 'like', "%$key%")
            ->orwhere('articles.title', 'like', "%$key%")
            ->orwhere('articles.content', 'like', "%$key%")
            ->orwhere('articles.address', 'like', "%$key%")
            ->orwhere('articles.status', 'like', "%$key%")
            ->orwhere('category_articles.name', 'like', "%$key%")
            ->paginate(5);
        return $articles;
    }

}
