<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StatusRule implements Rule
{

    public function __construct()
    {
        //
    }


    public function passes($attribute, $value)
    {
        return (($value == 1) || ($value == 0));
    }


    public function message()
    {
        return 'The :attribute must 0 or 1';
    }
}
