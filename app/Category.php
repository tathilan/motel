<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = 'category_articles';
    protected $fillable = ['id', 'name'];

    public function article()
    {
        return $this->hasMany('App\Article', 'category_id', 'id');
    }

    public function showCategoryArticle()
    {
        $categorys = Category::paginate(5);
        return $categorys;
    }

    public function addCategoryArticle($input)
    {
        $category = new Category();
        $category->id = $input->id;
        $category->name = $input->name;
        $category->save();
        return $category;
    }

    public function updateCategoryArticle($input, $id)
    {
        $category = Category::findOrFail($id);
        $category->name = $input->name;
        $category->save();

        return $category;
    }

    public function deleteCategoryArticle($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return $category;
    }

    public function searchCategoryArticle($key)
    {
        $categorys = Category::where('category_articles.name', 'like', "%$key%")
            ->paginate(5);
        return $categorys;
    }

    public function showListArticle($id)
    {
        $articles = Article::select('articles.*')
            ->join('category_articles', 'articles.category_id', '=', 'category_articles.id')
            ->where('articles.category_id', '=', $id)
            ->get();
        return $articles;
    }
}
