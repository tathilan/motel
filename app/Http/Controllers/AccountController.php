<?php

namespace App\Http\Controllers;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    function getRegisterAdmin()
    {
        return view('admin.register');
    }

    public function postRegisterAdmin(RegisterRequest $request)
    {
        $user = new User;
        $user->id = $request->id;
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->level = 1;
        $user->save();

        return redirect('admin/register')->with('notification', 'Register Success!');
    }

    function getLoginAdmin()
    {
        return view('admin.login');
    }

    public function postLoginAdmin(LoginRequest $request)
    {

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect('admin/categories');
        } else {
            return redirect('admin/login')->with('notification', 'Login unsuccessful!');
        }

    }

}
