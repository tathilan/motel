<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Http\Requests\ArticleRequest;
use Illuminate\Http\Request;

class ArticleController extends Controller
{

    function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function index(Request $request)
    {
        $categories = Category::all();
        $articles = $this->article->showArticle();
        return view('admin.article.list', ['categories' => $categories, 'articles' => $articles]);

    }

    public function store(ArticleRequest $request)
    {
        $categories = Category::all();
        $article = $this->article->addArticle($request);
        return response()->json(['article' => $article, 'categories' => $categories]);
    }


    public function update(ArticleRequest $request, $id)
    {
        $categories = Category::all();
        $article = $this->article->updateArticle($request, $id);
        return response()->json(['article' => $article, 'categories' => $categories]);
    }

    public function destroy($id)
    {
        $article = $this->article->deleteArticle($id);
        return response()->json(['article' => $article]);
    }

    public function show(Request $request)
    {
        $key = $request->keyword;
        $categories = Category::all();
        $articles = $this->article->searchArticle($key);
        return view('admin.article.search', ['key' => $key, 'articles' => $articles, 'categories' => $categories]);
    }
}
