<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Http\Request;
use Validator;

class CategoryController extends Controller
{

    function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function index(Request $request)
    {
        $categories = $this->category->showCategoryArticle();
        return view('admin.category.list', compact('categories'));
    }


    public function store(CategoryRequest $request)
    {
        $category = $this->category->addCategoryArticle($request);
        return response()->json($category);
    }

    public function update(CategoryRequest $request, $id)
    {
        $category = $this->category->updateCategoryArticle($request, $id);
        return response()->json($category);
    }

    public function destroy($id)
    {
        $category = $this->category->deleteCategoryArticle($id);

        return response()->json($category);
    }

    public function show(Request $request)
    {
        $key = $request->keyword;
        $categories = $this->category->searchCategoryArticle($key);
        return view('admin.category.search', ['categories' => $categories, 'key' => $key]);
    }

    public function showArticle($id)
    {
        $categories = Category::all();
        $articles = $this->category->showListArticle($id);
        return view('admin.category.show', ['articles' => $articles, 'categories' => $categories]);
    }
}
