<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [

            'name' => 'required|max:128|min:2|unique:category_articles',
        ];
    }

    public function messages()
    {
        //notification from rules
        return [
            'name.required' => "Please, enter name category....",
            'name.min' => "At least two characters...",
            'name.max' => "Up to 128 characters.....",
            'name.unique' => ' Name already exist.....',

        ];
    }
}
