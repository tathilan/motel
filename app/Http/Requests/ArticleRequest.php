<?php

namespace App\Http\Requests;

use App\Rules\StatusRule;
use Illuminate\Foundation\Http\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:10',
            'content' => 'required|min:10',
            'address' => 'required|min:10',
            'phonenumber' => 'required|numeric',
            'status' => ['required', 'numeric', new StatusRule()],
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'Please, enter tilte of article....',
            'title.min' => 'Title: At least ten characters...',
            'content.required' => 'Please, enter content of article....',
            'content.min' => 'Content: At least ten characters...',
            'address.required' => 'Please, enter address of article....',
            'address.min' => 'Address: At least ten characters...',
            'phonenumber.required' => 'Please, enter phone number....',
            'phonenumber.numeric' => 'Phone number:Not is number',
            'status.required' => 'Please, Set status for article...',
            'status.numeric' => 'Status: Not is number...',

        ];
    }

}
