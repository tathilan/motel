<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'email' => 'required|email',
            'password' => 'required|min:3|max:32',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Please, Enter your email...',
            'email.email' => 'Please, Enter the correct email format...',
            'password.required' => 'Please, Enter your password...',
            'password.min' => 'Passsword: At least six characters...',
            'password.max' => 'Password: Up to 32 characters...'
        ];
    }
}
